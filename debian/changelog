pyqt5-sip (12.11.1-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 23:07:14 +0000

pyqt5-sip (12.11.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.6.2, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 28 Jan 2023 22:26:23 +0400

pyqt5-sip (12.11.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Avoid pypi.org in Homepage field.
  * Drop transition for old debug package migration.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 28 Oct 2022 18:46:56 +0100

pyqt5-sip (12.11.0-1) unstable; urgency=medium

  [ Nicholas D Steeves ]
  * gbp.conf: Add missing configuration for pristine-tar.

  [ Dmitry Shachnev ]
  * New upstream release.
  * Bump Standards-Version to 4.6.1, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 20 Jun 2022 20:01:36 +0300

pyqt5-sip (12.10.1-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on python3-setuptools.

  [ Dmitry Shachnev ]
  * New upstream release.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 16 Apr 2022 17:40:06 +0300

pyqt5-sip (12.9.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump years in debian/copyright.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 04 Feb 2022 18:44:22 +0300

pyqt5-sip (12.9.0-3) unstable; urgency=medium

  * Stop building extensions for Python debug interpreter (closes: #994338).
    - Rely on automatic dbgsym packages for debug symbols.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 06 Oct 2021 12:51:22 +0300

pyqt5-sip (12.9.0-2) unstable; urgency=medium

  * Bump Standards-Version to 4.6.0, no changes needed.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 05 Sep 2021 21:21:39 +0300

pyqt5-sip (12.9.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Dmitry Shachnev ]
  * New upstream release.
  * Bump years in debian/copyright.
  * Bump Standards-Version to 4.5.1, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 14 May 2021 20:09:54 +0300

pyqt5-sip (12.8.1-1apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 21 Apr 2021 11:18:29 +0200

pyqt5-sip (12.8.1-1) unstable; urgency=medium

  * New upstream release.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 29 Aug 2020 21:20:23 +0300

pyqt5-sip (12.8.0-1) unstable; urgency=medium

  * New upstream release.
  * Drop reproducible_extensions.diff, applied upstream.
  * Update to debhelper compat level 13.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 03 Jun 2020 20:49:32 +0300

pyqt5-sip (12.7.2-1) experimental; urgency=medium

  * Initial release (closes: #956113).

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 07 Apr 2020 19:11:33 +0300
